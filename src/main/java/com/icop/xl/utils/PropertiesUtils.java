package com.icop.xl.utils;

import com.icop.xl.config.XlConfigProperties;

/**
 * @author: liukj
 * @date: 2020/3/27
 * @description：
 */
public class PropertiesUtils {

    /**
     *  创建对象副本
     * @param globalConfiguration
     * @return
     */
    public static XlConfigProperties copy (XlConfigProperties globalConfiguration){
        XlConfigProperties xlConfigProperties = new XlConfigProperties();
        xlConfigProperties.setLimitNum(globalConfiguration.getLimitNum());
        xlConfigProperties.setLimitSwitch(globalConfiguration.getLimitSwitch());
        xlConfigProperties.setLimitPeriod(globalConfiguration.getLimitPeriod());
        xlConfigProperties.setTimeOut(globalConfiguration.getTimeOut());
        xlConfigProperties.setLimitType(globalConfiguration.getLimitType());
        xlConfigProperties.setPerSecond(globalConfiguration.getPerSecond());
        return xlConfigProperties;
    }


}
