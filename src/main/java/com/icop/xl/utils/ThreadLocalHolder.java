package com.icop.xl.utils;

/**
 * @author: liukj_mci
 * @date: 2019/6/25
 * @description： 线程使用数据工具
 */
public class ThreadLocalHolder {

    private static final ThreadLocal THREAD_LOCAL = new ThreadLocal();

    private ThreadLocalHolder() { }

    public static void setValue(Object obj){
        THREAD_LOCAL.set(obj);
    }

    public static Object getValue(){
        return ThreadLocalHolder.THREAD_LOCAL.get();
    }

    public static void removeValue(){
        ThreadLocalHolder.THREAD_LOCAL.remove();
    }

}
