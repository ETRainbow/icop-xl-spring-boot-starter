package com.icop.xl.enties;

import com.icop.xl.config.XlConfigProperties;

/**
 * @author: liukj
 * @date: 2020/4/3
 * @description：
 */
public class XlConstants {


    public static final int ZERO = 0;
    /**
     * 默认访问周期
     */
    public static final int DEFAULT_LIMIT_PERIOD = 30000;
    /**
     * 默认访问周期内的访问次数
     */
    public static final int DEFAULT_LIMIT_NUM = 3;

    /**
     * 该类型基于google的Guava RateLimiter限流实现，采用
     *   基于令牌同的稳定模式(SmoothBursty:令牌生成速度恒定)，通过设置令牌通每秒生成令牌的数量，对服务接口进行限流控制。
     *   如果设置了{@link XlConfigProperties#timeOut},则可以等待指定时长以获取令牌。
     */
    public static final String LIMIT_TYPE_PRE_SECOND="SECOND";

    /**
     * 该类型基于google的Guava中的cache实现，默认将访问者IP及其访问的服务名，
     *   作为访问的ID,对其再指定的周期{@link XlConfigProperties.limitPeriod}内的访问次数@{@link XlConfigProperties#limitNum}
     *   进行控制。
     */
    public static final String LIMIT_TYPE_DYNAMIC="DYNAMIC";

    /**
     *  默认每秒允许的最大流量
     */
    public static final Double PER_SECOND_MAX_RATE = 1000d;
    public static final int PER_SECOND_TIME_OUT = 0;

    /**
     * 限制开关
     */
    public static final String LIMIT_SWITCH_ON = "ON";
    public static final String LIMIT_SWITCH_OFF = "OFF";


}
