package com.icop.xl.enties;

import com.icop.xl.config.XlConfigProperties;
import lombok.Data;

/**
 * @author: liukj
 * @date: 2020/3/27
 * @description：
 */
@Data
public class XlParam {
    private XlConfigProperties xlConfigProperties;
    private String limitId;
}
