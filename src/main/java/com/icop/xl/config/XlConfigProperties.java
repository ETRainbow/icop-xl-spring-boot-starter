package com.icop.xl.config;

import com.icop.xl.enties.XlConstants;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;


@Data
@ConfigurationProperties(prefix = "icop.xl")
public class XlConfigProperties {


    String limitType = XlConstants.LIMIT_TYPE_PRE_SECOND;

    /**
     * 每秒向桶中放入令牌的数量   默认最大即不做限流(每秒允许的访问数量)
     * limitType = 1 时生效  !
     *
     * @return
     */
    double perSecond=XlConstants.PER_SECOND_MAX_RATE;

    /**
     * 获取令牌的等待时间  默认0，单位为毫秒
     *
     * @return
     */
    int timeOut=XlConstants.PER_SECOND_TIME_OUT;

    /**
     * 是否限流，默认关闭限流
     */
    private String limitSwitch = XlConstants.LIMIT_SWITCH_OFF;

    /**
     * 访问周期，单位为毫秒
     */
    private int limitPeriod = XlConstants.DEFAULT_LIMIT_PERIOD;

    /**
     * 访问周期内的最多访问次数，默认3次
     */
    private int limitNum = XlConstants.DEFAULT_LIMIT_NUM;

}
