package com.icop.xl.config;

import com.icop.xl.handler.IXlHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: liukj
 * @date: 2020/3/27
 * @description： 全局资源持有者
 */
@Slf4j
@EnableConfigurationProperties(XlConfigProperties.class)
@Configuration
public class GlobalResourcesHolder implements InitializingBean {

    /**
     * 全局限流配置，值为application.properties or yml文件中的配置数据
     */

    XlConfigProperties xlGlobalConfigProperties;

    GlobalResourcesHolder(XlConfigProperties xlGlobalConfigProperties){
        //log.info("GlobalResourcesHolder初始化=================");
        this.xlGlobalConfigProperties = xlGlobalConfigProperties;
    }

    private Map<String, IXlHandler> xlHandlerMap = new HashMap<>(2);

    public void addXlHandler(String limitType, IXlHandler xlHandler) {
        this.xlHandlerMap.put(limitType, xlHandler);
    }

    public IXlHandler obtainXlHandler(String limitType) {
        return this.xlHandlerMap.get(limitType);
    }

    public XlConfigProperties getXlGlobalConfigProperties() {
        return xlGlobalConfigProperties;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        //log.info("初始全局资源持有对象");
        Assert.notNull(xlGlobalConfigProperties,"限流全局资源初始化时，配置对象不能为空！");
    }
}


