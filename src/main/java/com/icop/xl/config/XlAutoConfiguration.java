package com.icop.xl.config;

import com.icop.xl.aspect.XlHandlerAspect;
import com.icop.xl.enties.XlConstants;
import com.icop.xl.handler.IXlHandler;
import com.icop.xl.handler.XlRateLimitHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author: liukj
 * @date: 2020/3/27
 * @description：
 */
@ConditionalOnWebApplication
@Configuration
@Import(GlobalResourcesHolder.class)
@Slf4j
public class XlAutoConfiguration{

    @Bean(name = "defaultXlRateLimitHandler")
    protected IXlHandler defaultXlRateLimitHandler(GlobalResourcesHolder globalResourcesHolder) {
        XlRateLimitHandler xlRateLimitHandler = new XlRateLimitHandler(globalResourcesHolder);
        globalResourcesHolder.addXlHandler(XlConstants.LIMIT_TYPE_PRE_SECOND,xlRateLimitHandler);
        return xlRateLimitHandler;
    }

    @Bean
    protected XlHandlerAspect xlHandlerAspect(GlobalResourcesHolder globalResourcesHolder) {
        return new XlHandlerAspect(globalResourcesHolder);
    }

}
