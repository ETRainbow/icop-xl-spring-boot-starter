package com.icop.xl.aspect;

import com.icop.xl.enties.XlConstants;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static com.icop.xl.enties.XlConstants.*;

/**
 * @author: liukj
 * @date: 2020/3/25
 * @description： 配置属性
 *
 *  限流配置有两个类型
 *  1.如果{@link XlHandler#limitType()}为{@link XlConstants#LIMIT_TYPE_PRE_SECOND}, 该类型基于google的Guava RateLimiter限流实现，采用
 *  基于令牌同的稳定模式(SmoothBursty:令牌生成速度恒定)，通过设置令牌通每秒生成令牌的数量，对服务接口进行限流控制。
 *  如果设置了{@link XlHandler#timeOut},则可以等待指定时长以获取令牌。
 *  如果类型为该类型，其参数只能再application.properties or yml 文件中配置，
 *  有效参数：
 *  {@link XlHandler#limitSwitch()} 开关控制，如果注解中有配置，其优先级高于application.properties or yml 文件中配置
 *  {@link XlHandler#perSecond()} 每秒中的访问量
 *  {@link XlHandler#timeOut()} ,获取令牌的可等待时间
 *
 *
 *  2.如果{@link XlHandler#limitType()}为{@link XlConstants#LIMIT_TYPE_DYNAMIC },该类型基于google的Guava中的cache实现，默认将访问者IP及其访问的服务名，
 *  作为访问的ID,对其再指定的周期{@link XlHandler#limitPeriod}内的访问次数@{@link XlHandler#limitNum}，该访问为平滑的访问控制。
 *  例如： limitPeriod = 60000 ， limitNum = 6，则1分钟内最多访问6次，且每10秒内只能访问1次
 *
 *  有效参数：
 *  {@link XlHandler#limitSwitch()} 开关控制，如果注解中有配置，其优先级高于application.properties or yml 文件中配置
 *  {@link XlHandler#limitPeriod()} 单位访问周期(单位为毫秒)
 *  {@link XlHandler#limitNum()} ,单位访问周期内的访问次数
 *
 *
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface XlHandler {

    /**
     * 是否访问限制！
     *
     * limitSwitch为OFF时，不启用访问限流控制，其优先级高于application.properties or yml 文件中配置
     */
    String limitSwitch() default "";


    /**
     * 限流处理类型，
     * SECOND： 类型为秒级限流，对每秒所允许访问的量进行控制。 (默认类型)
     * DYNAMIC： 为单位时间内的平滑访问量控制
     *
     * @return
     */
    String limitType() default "";

    /**
     * 每秒向桶中放入令牌的数量
     * limitType = {@link XlConstants#LIMIT_TYPE_PRE_SECOND}时有用
     *
     * @return
     */
    double perSecond() default 1000d;

    /**
     * 获取令牌的等待时间  默认0
     *
     * @return
     */
    int timeOut() default PER_SECOND_TIME_OUT;


    /**
     * limitType为0时，访问周期，默认为30秒钟
     */
    int limitPeriod() default ZERO;

    /**
     * limitType为0时，访问周期内的最多访问次数，默认3次
     */
    int limitNum() default ZERO;
}
