package com.icop.xl.aspect;

import com.icop.xl.config.GlobalResourcesHolder;
import com.icop.xl.config.XlConfigProperties;
import com.icop.xl.enties.XlConstants;
import com.icop.xl.enties.XlParam;
import com.icop.xl.handler.DynamicXlHandler;
import com.icop.xl.handler.IXlHandler;
import com.icop.xl.utils.HttpUtils;
import com.icop.xl.utils.PropertiesUtils;
import com.icop.xl.utils.ThreadLocalHolder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @author: liukj
 * @date: 2020/3/25
 * @description：
 */
@Aspect
@Slf4j
public class XlHandlerAspect implements InitializingBean {

   GlobalResourcesHolder globalResourcesHolder;
   public XlHandlerAspect(GlobalResourcesHolder globalResourcesHolder){
       this.globalResourcesHolder =  globalResourcesHolder;
   }

    @Before(value = ("@annotation(com.icop.xl.aspect.XlHandler)"))
    private void beforeHandler(JoinPoint joinPoint) {

        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        Method method = methodSignature.getMethod();

        XlHandler annotationHandler = method.getAnnotation(XlHandler.class);

        String accessId = getAccessId(joinPoint,method);
        //获取生效配置
        XlParam xlParam = obtainValidConfig(accessId, annotationHandler);

        if(!XlConstants.LIMIT_SWITCH_ON.equals(xlParam.getXlConfigProperties().getLimitSwitch())){
            return;
        }

        // 获取限流处理对象
        IXlHandler iXlHandler = obtainIXlHandler(xlParam);

        boolean isPermit = iXlHandler.handler(xlParam);
        if (!isPermit) {
            throw new RuntimeException("服务器繁忙，请稍后重试！");
        }
    }

    /**
     *  获取生效配置
     * @param accessId 访问者ID
     * @param annotationHandler 注解
     * @return
     */
    private XlParam obtainValidConfig(String accessId, XlHandler annotationHandler){
        XlParam xlParam = new XlParam();
        XlConfigProperties xlGlobalConfigProperties = globalResourcesHolder.getXlGlobalConfigProperties();
        XlConfigProperties copyProperties = PropertiesUtils.copy(xlGlobalConfigProperties);

        // 开关选取
        String limitSwitch = annotationHandler.limitSwitch();
        if(StringUtils.isBlank(limitSwitch)){
            // 注解上未配置，取全局配置，默认为OFF
            limitSwitch = globalResourcesHolder.getXlGlobalConfigProperties().getLimitSwitch();
        }
        copyProperties.setLimitSwitch(limitSwitch);

        // 处理类型选取
        String limitType = annotationHandler.limitType();
        if(StringUtils.isBlank(limitType)){
            limitType = globalResourcesHolder.getXlGlobalConfigProperties().getLimitType();
        }
        copyProperties.setLimitType(limitType);

        if(XlConstants.LIMIT_TYPE_DYNAMIC.equals(limitType)){
            setAnnotationConfigurationOfDynamic(annotationHandler,limitType,copyProperties);
        }

        xlParam.setLimitId(accessId);
        xlParam.setXlConfigProperties(copyProperties);
        return xlParam;
    }

    /**
     * 根据处理类型，获取处理对象
     *
     * @param xlParam
     * @return
     */
    private IXlHandler obtainIXlHandler(XlParam xlParam) {
        XlConfigProperties config = xlParam.getXlConfigProperties();
        String limitType = config.getLimitType();

        if(XlConstants.LIMIT_TYPE_PRE_SECOND.equals(limitType)){
            IXlHandler iXlHandler = globalResourcesHolder.obtainXlHandler(limitType);
            if(null == iXlHandler){
                throw new RuntimeException("为获取限流类型["+limitType+"]对应的处理者！");
            }
            return iXlHandler;
        }

        if(XlConstants.LIMIT_TYPE_DYNAMIC.equals(limitType)){
            int limitPeriod = config.getLimitPeriod();
            int limitNum = config.getLimitNum();

            String handlerKey= limitType+"-"+limitPeriod+"-"+limitNum;
            IXlHandler iXlHandler = globalResourcesHolder.obtainXlHandler(handlerKey);

            if(null == iXlHandler){
                iXlHandler= new DynamicXlHandler(limitPeriod,limitNum);
                // 第一次访问时，初始化相同配置的限流处理对象
                globalResourcesHolder.addXlHandler(handlerKey,iXlHandler);
            }

            return iXlHandler;
        }

        throw new RuntimeException("E_00_00:该限流类型["+limitType+"]不支持！");
    }

    /**
     * 获取访问的ID,ID访问格式：IP地址-访问的服务名
     * @param joinPoint 切点
     * @param method 访问的服务名
     * @return
     */
    private String getAccessId(JoinPoint joinPoint, Method method) {
        HttpServletRequest httpServletRequest = obtainServletRequest(joinPoint);
        String requestIp = HttpUtils.getRequestIp(httpServletRequest);
        String methodName = method.getName();
        return requestIp+"-"+methodName;
    }

    /**
     * 判断请求参数中是否含有HttpServletRequest,如果有，即返回该对象
     *
     * @param joinPoint
     * @return
     */
    private HttpServletRequest obtainServletRequest(JoinPoint joinPoint) {
        // 获取注解中的参数列表
        Object[] args = joinPoint.getArgs();
        boolean isSupport;
        for (int i = 0; i < args.length; i++) {
            Object arg = args[i];
            Class<?>[] interfaces = arg.getClass().getInterfaces();
            isSupport = Arrays.stream(interfaces).collect(Collectors.toList()).contains(HttpServletRequest.class);
            if (isSupport) {
                return (HttpServletRequest) arg;
            }
        }
        return null;
    }


    /**
     * 优先获取注解中的限流设置,默认取application.yml或properties中的配置
     *
     * @param annotation 注解
     * @param limitType 处理类型
     * @param copyProperties 配置
     */
    private void setAnnotationConfigurationOfDynamic(XlHandler annotation,String  limitType,XlConfigProperties copyProperties) {

        if (XlConstants.LIMIT_TYPE_DYNAMIC.equals(limitType)) {
            // 获取特定类型注解上的单独配置
            int limitNum = annotation.limitNum();
            int limitPeriod = annotation.limitPeriod();

            if (XlConstants.ZERO != limitNum) {
                copyProperties.setLimitNum(limitNum);
            }
            if (XlConstants.ZERO != limitPeriod) {
                copyProperties.setLimitPeriod(limitPeriod);
            }
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(globalResourcesHolder,"限流全局资源持有者为空！");
    }
}
