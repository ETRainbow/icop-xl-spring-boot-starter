package com.icop.xl.handler;

import com.icop.xl.enties.XlParam;

/**
 * @author: liukj
 * @date: 2020/3/26
 * @description：
 */
public abstract class AbstractXlHandler implements IXlHandler {

    @Override
    public boolean handler(XlParam xlParam) {
        String limitType = xlParam.getXlConfigProperties().getLimitType();
        boolean support = this.isSupport(limitType);

        if (support) {
            return this.handlerImpl(xlParam);
        } else {
            return true;
        }
    }

    /**
     * 详细的限流处理
     *
     * @return
     */
    abstract boolean handlerImpl(XlParam xlParam);

}
