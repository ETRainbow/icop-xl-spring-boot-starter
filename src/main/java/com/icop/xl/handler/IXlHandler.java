package com.icop.xl.handler;

import com.icop.xl.enties.XlParam;

/**
 * @author: liukj
 * @date: 2020/3/25
 * @description：
 */
public interface IXlHandler {

    /**
     * 限流处理逻辑，允许访问，
     * 返回true,否正为false
     *
     * @return
     */
    boolean handler(XlParam xlParam);

    /**
     * 是否支持该类型的处理，
     * true 支持，否正不支持
     *
     * @return
     */
    boolean isSupport(String limitType);


}
