package com.icop.xl.handler;

import com.google.common.util.concurrent.RateLimiter;
import com.icop.xl.config.GlobalResourcesHolder;
import com.icop.xl.enties.XlConstants;
import com.icop.xl.enties.XlParam;

import java.util.concurrent.TimeUnit;

/**
 * @author: liukj
 * @date: 2020/3/26
 * @description： 基于google的Guava  包下面的令牌桶算法，实现限流操作
 */

public class  XlRateLimitHandler extends AbstractXlHandler {

    RateLimiter rateLimiter ;

    public XlRateLimitHandler(GlobalResourcesHolder globalResourcesHolder) {
        rateLimiter = RateLimiter.create(globalResourcesHolder.getXlGlobalConfigProperties().getPerSecond());
    }

    @Override
    boolean handlerImpl(XlParam xlParam) {
        /*GlobalResourcesHolder globalResourcesHolder = getGlobalResourcesHolder();
        XlConfigProperties xlGlobalConfigProperties = globalResourcesHolder.getXlGlobalConfigProperties();*/
        int timeOut = xlParam.getXlConfigProperties().getTimeOut();
        if (rateLimiter.tryAcquire(timeOut, TimeUnit.MILLISECONDS)) {
            return true;
        }
        return false;
    }
    @Override
    public boolean isSupport(String limitType) {
        return XlConstants.LIMIT_TYPE_PRE_SECOND.equals(limitType);
    }
}
