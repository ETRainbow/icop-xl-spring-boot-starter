package com.icop.xl.handler;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.icop.xl.enties.XlConstants;
import com.icop.xl.enties.XlParam;
import org.apache.commons.lang3.StringUtils;

import java.util.concurrent.TimeUnit;

/**
 *
 *  基于谷歌的Guava Cache 实现访问记录的存储，
 *
 *
 * @author: liukj
 * @date: 2020/3/25
 * @description：
 */
public class DynamicXlHandler extends AbstractXlHandler {

    /**
     *  访问周期时间
     */
    long limitPeriod ;
    /**
     *  访问周期内，可以访问的总次数
     */
    int limitNum ;
    Cache<String, String> cache;

    public DynamicXlHandler(int limitPeriod, int limitNum) {
        initCache(limitPeriod,limitNum);
    }

    /**
     *  缓存设置初始化
     * @param limitPeriod 限制的访问时间段
     * @param limitNum 限制的访问时间段内，最大的访问次数
     * @return
     */
    private void initCache (int limitPeriod, int limitNum){
        this.limitNum = limitNum;
        this.limitPeriod = limitPeriod;

        // 通过CacheBuilder构建一个缓存实例
        cache = CacheBuilder.newBuilder()
                // 设置缓存的最大容量
                //.maximumSize(limitNum)
                // 设置缓存在写入多少时间后失效，这里存在一个特殊情况：如果刚好再过期前最后一次访问，那么下次访问，必须等一个限制周期
                .expireAfterWrite(limitPeriod, TimeUnit.MILLISECONDS)
                // 设置并发级别为10
                .concurrencyLevel(10)
                // 开启缓存统计
                .recordStats()
                .build();
    }

    @Override
    boolean handlerImpl(XlParam xlParam) {

        String limitId = xlParam.getLimitId();

        // 获取缓存中的记录
        String[] accessRecorder = getAccessRecorder(limitId);

        long firstAccessTimeMillis = Long.parseLong(accessRecorder[0]);
        long accessNum = Long.parseLong(accessRecorder[1]);
        long currentTimeMillis = System.currentTimeMillis();

        // 主动判断访问是否已过一个访问周期控制,如果过了，则主动清楚缓存记录,记录归0
        if(0 != firstAccessTimeMillis){
            if((currentTimeMillis - firstAccessTimeMillis) >= limitPeriod){
                cache.invalidate(limitId);
                firstAccessTimeMillis = 0;
                accessNum = 0;
            }
        }

        // 判断是否超过最大的允许访问数量
        boolean inAllowAccessNum = isInAllowAccessNum(accessNum);

        if(inAllowAccessNum){
            // 判断是否再单位时间内多此访问
            boolean inAccessTime = isInAccessTime(firstAccessTimeMillis,accessNum);

            if(inAccessTime){

                String cacheValue;
                accessNum ++ ;
                //第一次访问
                if(0==firstAccessTimeMillis){
                    cacheValue= ""+currentTimeMillis+"-"+accessNum;
                }else{
                    cacheValue = ""+firstAccessTimeMillis+"-"+accessNum;
                }
                //更新缓存时间
                cache.put(limitId,cacheValue);
                return true;
            }
        }
       return false;
    }


    /**
     *  判断是否处于单位访问时间之外
     *  单位访问时间内，只允许一次访问
     * @param firstAccessTime 第一次访问的时间点
     * @param lastAccessNum 已经访问的数量
     * @return true为在单位访问时间内之外
     */
    private boolean isInAccessTime(long firstAccessTime,long lastAccessNum){
        long currentTimeMillis = System.currentTimeMillis();

        if(0 == firstAccessTime){
            return true;
        }

        //计算平滑的 访问时间
        long gap = limitPeriod / limitNum ;
        return (currentTimeMillis - firstAccessTime) > (lastAccessNum * gap);
    }

    /**
     * 判断是否再允许访问的总次数以内
     *
     * @param lastNum 上次访问的次数
     * @return true为可以访问
     */
    private boolean isInAllowAccessNum(long lastNum){
        return limitNum > lastNum ? true : false ;
    }

    /**
     *  获取访问记录
     *
     * @param limitId 限制ID
     * @return
     */
    private String[] getAccessRecorder (String limitId){
        // 格式："第一次访问时间"-"访问次数"
        String lastRecord = cache.getIfPresent(limitId);

        if(StringUtils.isBlank(lastRecord)){
            lastRecord = "0-0";
        }
        return lastRecord.split("-");
    }

    @Override
    public boolean isSupport(String limitType) {
        return XlConstants.LIMIT_TYPE_DYNAMIC.equals(limitType);
    }
}
