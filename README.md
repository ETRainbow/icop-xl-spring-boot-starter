## 关于icop-xl-spring-boot-starter 起步依赖

### 功能简述： 
对http访问请求进行限流处理。可根据注解和application.properties(yml)文件进行配置，其中注解配置的优先级高于文件配置

### 配置说明

![image.png](https://i.loli.net/2020/04/08/KoVNTradPpbBZ5e.png)

### 详细描述：

#### 一、限制开关
开关值：<br/>
1.ON ：打开限流控制 <br/>
2.OFF ：关闭<br/>
> 如果注解和application.properties(yml)文件都进行配置，其中注解配置的优先级高于文件配置,关闭状态不会进行限流操作

#### 一.限流类型
限制类型有两个：<br/>
1.秒级处理类型：SECOND （默认类型）  <br/>
2.动态处理类型：DYNAMIC <br/>
> SECOND 类型： <br/>
>其生效的参数配置为<br/>
>icop.xl.per-second、icop.xl.time-out，且只能再application.properties(yml)文件都进行配置，注解配置不生效 <br/>
>该类型基于google的Guava RateLimiter限流实现，基于漏桶算法，令牌的产生使用稳定模式(SmoothBursty:令牌生成速度恒定)，通过设置令牌通每秒生成令牌的数量，对服务接口进行限流控制。
><br/>
><br/>
> DYNAMIC 类型： <br/>
>其生效的参数配置为<br/>
>limitNum、limitPeriod，注解和application.properties(yml)文件都进行配置，其中注解配置的优先级高于文件配置,关闭状态不会进行限流操作<br/>
>该类型基于google的Guava中的cache实现，默认将访问者IP及其访问的服务名，作为访问的ID,对其指定的周期内的访问次数进行控制，该访问为平滑的访问控制。<br/>
>例如： limitPeriod = 60000 ， limitNum = 6，则1分钟内最多访问6次，且每10秒内只能访问1次

